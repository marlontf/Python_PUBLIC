# -*- coding: utf-8 -*-
import re
import smtplib
import dns.resolver, dns.name
import os
os.system('echo on')

# Endereco utilizado no campo DE do SMTP MAIL
fromAddress = 'teste@gmail.com'

# Sintaxe Regex simples para validacao do email
regex = '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$'

# Endereco de email para verificar
name = os.name
if name == 'posix':
    os.system('clear')
elif name == 'nt' or name == 'dos':
    os.system('cls')
else:
    print("\n" * 30)

email = input('Informe o e-mail para verificação:')
addressToVerify = str(email)

# Syntax check
match = re.match(regex, addressToVerify)
if match == None:
	print('E-mail Inválido')
	exit(0)
	# raise ValueError('E-mail Inválido')
		
# Get domain for DNS lookup
splitAddress = addressToVerify.split('@')
domain = str(splitAddress[1])
print('Domain:', domain)


# MX record lookup
records = dns.resolver.query(domain, 'MX')
mxRecord = records[0].exchange
mxRecord = str(mxRecord)

# SMTP lib setup (use debug level for full output)
server = smtplib.SMTP()
server.set_debuglevel(0)

# SMTP Conversation
server.connect(mxRecord)
server.helo(server.local_hostname) ### server.local_hostname(Get local server hostname)
server.mail(fromAddress)
code, message = server.rcpt(str(addressToVerify))
server.quit()

#print(code)
#print(message)

# Assume SMTP response 250 is success
if code == 250:
	print('\033[32m \033[1m (V) E-mail válido \033[0;0m')
else:
	print('\033[31m \033[1m (X) E-mail inválido \033[0;0m')

print('\n\n')
fechar=input('pressione ENTER para finalizar')