from bs4 import BeautifulSoup
import requests
import re
from getpass import getpass
import os
import string
os.system('echo on')
os.system('cls')

# Pegando informações de Login
vemail= input('Informe seu usuário:')
vpass = getpass('Informe sua senha:')

# Entrando na página de login do facebook e pegando inputs (Inclusive os hidden)
url = requests.get('https://m.facebook.com/login.php')
soup = BeautifulSoup(url.content, 'html.parser')
form = soup.find('form')
inputs = form.find_all('input')

# Pegando dados dos input hidden e jogando em variáveis
vlsd = soup.find('input', {'name': 'lsd'}).get('value')
vm_ts = soup.find('input', {'name': 'm_ts'}).get('value')
vli = soup.find('input', {'name': 'li'}).get('value')
vtry_number = soup.find('input', {'name': 'try_number'}).get('value')
vunrecognized_tries = soup.find('input', {'name': 'unrecognized_tries'}).get('value')
v_fb_noscript = soup.find('input', {'name': '_fb_noscript'}).get('value')

# Setando dados para submit do form
data = {
    'lsd': vlsd,
    'm_ts': vm_ts, 
    'li': vli,
    'try_number': vtry_number,
    'unrecognized_tries': vunrecognized_tries,
    '_fb_noscript': v_fb_noscript,
    'email': vemail,
    'pass': vpass,
    'login': 'login',
}

# Enviando dados por método POST
s = requests.session()
r = s.post('https://m.facebook.com/login.php', data=data)
soup = BeautifulSoup(r.content, 'html.parser')
print(soup)
vcheck = soup.select('span')[1]
vcheck = str(vcheck)
vcheck = vcheck.split('>')[1]
vcheck = vcheck.split('. ')[0]
if vcheck == 'A senha que você inseriu está incorreta':
    print('\033[31m \033[1mNome de usuário e/ou senha invalido(s). \n Favor verificar as informações e tentar novamente \033[0;0m')
else:
    os.system('cls')
    print(soup)
#    videntidade = soup.select("a")[4]
#    vamizade = str(soup.select('a')[6].text)
#    vmensagem = str(soup.select('a')[7].text)
#    vsolicitacoes = str(soup.select('a')[13].text)
#    print('\033[32m\033[1m(V) Login efetuado com sucesso \n\n')
#    print('Seja bem vindo ' + videntidade.text)
#    print('Você possui: ' + vamizade + ' ' + vmensagem + '\033[0;0m')